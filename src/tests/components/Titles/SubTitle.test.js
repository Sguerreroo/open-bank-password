import React from 'react';
import { Provider } from 'react-redux';

import { mount } from 'enzyme';

import { store } from '../../../app/store';
import SubTitle from '../../../components/Titles/SubTitle';

import i18next from 'i18next';

i18next.init({});

describe('Testing <SubTitle /> component', () => {

  const wrapper = mount(
    <Provider store={store}>
      <SubTitle/>
    </Provider>
  );

  test('should render <SubTitle /> component successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('should have some classes', () => {
    const node = wrapper.find('.subtitle-container');
    expect(node.exists()).toBe(true);
  });
});
