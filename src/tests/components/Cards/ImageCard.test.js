import React from 'react';

import { shallow } from 'enzyme';
import ImageCard from '../../../components/Cards/ImageCard';

describe('Testing <ImageCard /> component', () => {

  const image = 'https:www.somehost.com/path/to/image';
  const imgAlt = 'Alt text';
  const text = 'Some example text';
  const wrapper = shallow(
    <ImageCard image={image} imgAlt={imgAlt} text={text} />
  );

  test('should render <ImageCard /> component successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('should have an image with url and alt text equals to props', () => {
    const img = wrapper.find('img');
    expect(img.prop('src')).toBe(image);
    expect(img.prop('alt')).toBe(imgAlt);
  });

  test('should have a paragraph with a text', () => {
    const p = wrapper.find('p');
    expect(p.text().trim()).toBe(text);
  });

  test('should have a image-card-container class', () => {
    const div = wrapper.find('div');
    const className = div.prop('className');
    expect(className.includes('image-card-container')).toBe(true);
  });

});
