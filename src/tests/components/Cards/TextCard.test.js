import React from 'react';

import { shallow } from 'enzyme';
import TextCard from '../../../components/Cards/TextCard';

describe('Testing <TextCard /> component', () => {

  const title = 'One title';
  const text = 'Some text';
  const wrapper = shallow(
    <TextCard title={title} text={text} />
  );

  test('should render <TextCard /> component successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('should have a header with a title', () => {
    const h4 = wrapper.find('h4');
    expect(h4.text().trim()).toBe(title);
  });

  test('should have a paragraph with a text', () => {
    const p = wrapper.find('p');
    expect(p.text().trim()).toBe(text);
  });

  test('should have a text-card-container class', () => {
    const div = wrapper.find('div');
    const className = div.prop('className');
    expect(className.includes('text-card-container')).toBe(true);
  });


});
