import { submitForm } from '../../services/api';

describe('Testing mock api', () => {

  const TEST_KO = 'pruebaKO123';

  test('should return 401 status', async () => {
    try {
      const result = await submitForm(TEST_KO);
    } catch (error) {
      expect(error.status).toBe(401);
    }
  });

  test('should return 200 status', async () => {
    const password = '123Gsd9s3j';
    const result = await submitForm(password);
    expect(result.status).toBe(200);
  });

});
