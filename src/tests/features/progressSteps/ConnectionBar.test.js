import React from 'react';

import { shallow } from 'enzyme';
import ConnectionBar from '../../../features/progressSteps/ConnectionBar';

describe('Testing <ConnectionBar /> component', () => {

  test('should render <ConnectionBar /> component successfully', () => {
    const wrapper = shallow(<ConnectionBar visibility passedStep />);
    expect(wrapper).toMatchSnapshot();
  });

  test('should have a hidden class if not receive visibility prop', () => {
    const wrapper = shallow(<ConnectionBar />);
    const div = wrapper.find('div');
    expect(div.hasClass('hidden')).toBe(true);
  });

  test('should have a passed class if receive passedStep prop', () => {
    const wrapper = shallow(<ConnectionBar passedStep />);
    const div = wrapper.find('div');
    expect(div.hasClass('passed')).toBe(true);
  });

});
