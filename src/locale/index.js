import es from './es/es.json';
import en from './en/en.json';

export default { es, en };