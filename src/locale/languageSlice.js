import { createSlice } from '@reduxjs/toolkit';
import { LANGUAGE_CODES } from '../constants/LanguageCodes';

const initialState = {
  selectedLanguage: LANGUAGE_CODES.EN,
};

export const languageSlice = createSlice({
  name: 'language',
  initialState,
  reducers: {
    changeLanguage: (state, action) => {
      const language = action.payload;
      state.selectedLanguage = language;
    },
  }
});

export const {
  changeLanguage
} = languageSlice.actions;

export default languageSlice.reducer;