import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { useSelector } from 'react-redux';
import {
	Routes, Route, useNavigate
} from "react-router-dom";

import ProductInformation from '../views/ProductInformation/ProductInformation';
import CreatePasswordForm from '../views/Form/CreatePasswordForm';
import FeedbackMessage from '../views/Feedback/FeedbackMessage';
import StepBar from '../features/progressSteps/StepBar';

import { ROUTES } from '../constants/Routes';
import { STEP_PAGE } from '../constants/StepPages';
import { HTTP_STATUS } from '../constants/HttpStatus';

import './OpenBankApp.scss';

function OpenBankApp() {

	const { t } = useTranslation();

	const navigate = useNavigate();

	const { currentStep } = useSelector(state => state.step);
	const { status } = useSelector(state => state.passwordManager);

	// Navigate to the step page selected
	useEffect(() => {
		navigate(STEP_PAGE[currentStep]);
	}, [currentStep, navigate]);

	return (
		<div className='app'>
			<StepBar />
			<Routes>
				<Route
					path={ROUTES.home}
					element={<ProductInformation />}
				/>
				<Route
					path={ROUTES.createPassword}
					element={<CreatePasswordForm />}
				/>
				<Route
					path={ROUTES.feedback}
					element={
						status === HTTP_STATUS.succeed
							? (
								<FeedbackMessage
									title={t('titleSucceeded')}
									text={t('textSucceeded')}
									success
								/>
							)
							: (
								<FeedbackMessage
									title={t('titleFailed')}
									text={t('textFailed')}
								/>
							)
					}
				/>
			</Routes>
		</div>
	);
}

export default OpenBankApp;