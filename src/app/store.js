import { configureStore } from '@reduxjs/toolkit'
import stepReducer from '../features/progressSteps/StepSlice';
import passwordManagerReducer from '../features/password/PasswordSlice';
import languageReducer from '../locale/languageSlice';

export const store = configureStore({
  reducer: {
    step: stepReducer,
    passwordManager: passwordManagerReducer,
    language: languageReducer,
  },
});