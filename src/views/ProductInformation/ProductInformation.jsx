import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'react-i18next';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';

import { resetPasswordManager } from '../../features/password/PasswordSlice';
import ImageCard from '../../components/Cards/ImageCard';
import TextCard from '../../components/Cards/TextCard';
import SubTitle from '../../components/Titles/SubTitle';
import Footer from '../../components/Footer/Footer';
import Button from '../../components/Buttons/Button';

import { COLORS } from '../../constants/Buttons';
import { HTTP_STATUS } from '../../constants/HttpStatus';

import './ProductInformation.scss';

import Group3 from '../../assets/img/group-3.svg';
import Group from '../../assets/img/group.svg';

function ProductInformation() {

	const dispatch = useDispatch();

	const { t } = useTranslation();

	const { status } = useSelector(state => state.passwordManager);

	const [privacyPolicyAccepted, setPrivacyPolicyAccepted] = useState(false);

	// Reset password manager in step 1
	useEffect(() => {
		if (status !== HTTP_STATUS.idle)
			dispatch(resetPasswordManager())
	}, [dispatch, status]);

	const handleTogglePrivacyPolicy = e => {
		setPrivacyPolicyAccepted(e.target.checked);
	}

	return (
		<div className='product-information-container'>
			<header className='header'>
				<h1 className='header-title'>
					{t('welcomeTitle')}
				</h1>
				<SubTitle />
			</header>
			<main>
				<section className='image-section'>
					<ImageCard
						image={Group}
						imgAlt={t('alternativeTextBrain')}
						text={t('textBrain')}
					/>
					<ImageCard
						image={Group3}
						imgAlt={t('alternativeStrongbox')}
						text={t('textMasterKey')}
					/>
				</section>
				<section className='text-section'>
					<TextCard
						text={t('textCreatePassword')}
						title={t('titleCreatePassword')}
					/>
					<TextCard
						text={t('textInformationExample')}
						title={t('titleInformationExample')}
					/>
					<form onSubmit={e => e.preventDefault}>
						<label htmlFor="privacy-policy">
							{t('nextStepsInfo')}<br />{t('confirmationText')}<span className='privacy-policy-link'>{t('dataProtectionPolicyText')}</span>
						</label>
						<input
							type="checkbox"
							name="privacy-policy"
							id="privacy-policy"
							checked={privacyPolicyAccepted}
							onChange={handleTogglePrivacyPolicy}
						/>
					</form>
				</section>
			</main>
			<Footer
				backButton={
					<Button
						text={t('cancelButtonText')}
						color={COLORS.secondary}
						back
					/>
				}
				forwardButton={
					<Button
						text={t('nextButtonText')}
						color={COLORS.secondary}
						icon={faAngleRight}
						fill
						disabled={!privacyPolicyAccepted}
					/>
				}
			/>
		</div>
	);
}

export default ProductInformation;
