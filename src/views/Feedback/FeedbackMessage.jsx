import React from 'react';

import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCheckCircle, faExclamationTriangle, faAngleRight
} from '@fortawesome/free-solid-svg-icons';

import Button from '../../components/Buttons/Button';

import { incrementStep } from '../../features/progressSteps/StepSlice';

import { COLORS } from '../../constants/Buttons';

import './FeedbackMessage.scss';

function FeedbackMessage({ title, text, success = false }) {

  const { t } = useTranslation();

  return (
    <div className="feedback-message-container">
      <div className="feedback-message-wrapper">
        <main className='main'>
          <div
            className='icon-container'
            style={{
              color: `${success ? 'lightgreen' : 'red'}`
            }}
          >
            <div className='icon-wrapper'>
              {
                success
                  ? (
                    <FontAwesomeIcon
                      className='icon check-circle'
                      icon={faCheckCircle}
                    />
                  )
                  : (
                    <FontAwesomeIcon
                      className='icon exclamation-triangle'
                      icon={faExclamationTriangle}
                    />
                  )
              }
            </div>
          </div>
          <div className="text-container">
            <h2>{title}</h2>
            <p>{text}</p>
          </div>
        </main>
        <footer className='footer'>
          {
            success
              ? (
                <Button
                  text={t('logInButtonText')}
                  color={COLORS.primary}
                  icon={faAngleRight}
                  action={() => incrementStep()}
                />
              )
              : (
                <Button
                  text={t('backPasswordManagerButtonText')}
                  color={COLORS.primary}
                  icon={faAngleRight}
                  action={() => incrementStep()}
                />
              )
          }
        </footer>
      </div>
    </div>
  );
}

FeedbackMessage.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default FeedbackMessage;
