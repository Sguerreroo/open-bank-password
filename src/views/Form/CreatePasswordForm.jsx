import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Trans, useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faInfoCircle, faEye, faEyeSlash, faAngleRight
} from '@fortawesome/free-solid-svg-icons'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import { submitPassword } from '../../features/password/PasswordSlice';
import SubTitle from '../../components/Titles/SubTitle';
import Footer from '../../components/Footer/Footer';
import Button from '../../components/Buttons/Button';
import Spinner from '../../components/Spinner/Spinner';

import { COLORS } from '../../constants/Buttons';
import { HTTP_STATUS } from '../../constants/HttpStatus';
import { PASSWORD_LEVELS } from '../../constants/PasswordLevels';

import './CreatePasswordForm.scss';

const passwordSchema = yup.object().shape({
  password: yup.string()
    .matches(/\d/, 'passwordOneNumber')
    .matches(/[A-Z]/, 'passwordCapitalLetter')
    .min(8, 'passwordMinChar')
    .max(24, 'passwordMaxChar')
    .required(),
  repeatedPassword: yup.string()
    .oneOf([yup.ref('password'), null], 'notEqualPasswords'),
  hint: yup.string().max(255, 'hintMaxLength')
});

function CreatePasswordForm() {

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { status } = useSelector(state => state.passwordManager);

  const {
    register, handleSubmit, formState: { errors, isSubmitted }, watch,
  } = useForm({
    resolver: yupResolver(passwordSchema)
  });

  const hint = watch('hint');
  const password = watch('password');
  
  const onSubmit = data => {
    const { password, hint } = data;
    dispatch(submitPassword({ password, hint }));
  };

  const submitForm = e => {
    e.preventDefault();
    handleSubmit(onSubmit)();
  };

  // Fields visibility
  const [showPassword, setShowPassword] = useState(false);
  const [showRepeatedPassword, setShowRepeatedPassword] = useState(false);

  return (
    <div className='form-container'>
      <header className='header'>
        <SubTitle />
      </header>
      <main>
        <section className='information'>
          <Trans i18nKey="createPasswordManagerIntro">
            <p>
              En primer lugar, debes crear una contraseña diferente para sus pertenencias electrónicas.
              <br />
              No podrás recuperar tu contraseña, así que recuérdela bien.
            </p>
          </Trans>
        </section>
        <section>
          <form
            className='form'
            onSubmit={e => e.preventDefault()}
          >
            <div className='password-group'>
              <div className='password'>
                <label>{t('createMasterPassword')}</label>
                <div className='input-wrapper'>
                  <input
                    type={showPassword ? 'text' : 'password'}
                    {...register("password")}
                  />
                  {
                    showPassword
                      ? (
                        <FontAwesomeIcon
                          onClick={() => setShowPassword(false)}
                          className='icon eye-slash'
                          icon={faEyeSlash}
                        />
                      )
                      : (
                        <FontAwesomeIcon
                          onClick={() => setShowPassword(true)}
                          className='icon eye'
                          icon={faEye}
                        />
                      )
                  }
                  {
                    password?.length > 0 && errors?.password?.message &&
                    <div
                      style={{
                        ...PASSWORD_LEVELS[errors.password.message]
                      }}
                      className='password-strength'
                    ></div>
                  }
                  {
                    !errors?.password && password && isSubmitted &&
                    <div
                      className='password-strength success'
                    ></div>
                  }
                </div>
                <div className="password-rules">
                  {errors && t(errors.password?.message)}
                </div>
              </div>
              <div className='repeated-pass'>
                <label>{t("repeatMasterPassword")}</label>
                <div className='input-wrapper'>
                  <input
                    type={showRepeatedPassword ? 'text' : 'password'}
                    {...register("repeatedPassword")}
                  />
                  {
                    showRepeatedPassword
                      ? (
                        <FontAwesomeIcon
                          onClick={() => setShowRepeatedPassword(false)}
                          className='icon eye-slash'
                          icon={faEyeSlash}
                        />
                      )
                      : (
                        <FontAwesomeIcon
                          onClick={() => setShowRepeatedPassword(true)}
                          className='icon eye'
                          icon={faEye}
                        />
                      )
                  }
                </div>
                <div className="password-rules">
                  {errors && t(errors.repeatedPassword?.message)}
                </div>
              </div>
            </div>
            <div className='hint-group'>
              <p>
                {t('masterPasswordHint')}
              </p>
              <div className='hint'>
                <label>
                  {t('createMasterPasswordHint')}
                  <FontAwesomeIcon
                    className='info-icon'
                    icon={faInfoCircle}
                  />
                </label>
                <div className='input-wrapper'>
                  <input type="text" {...register("hint")} />
                </div>
                <div className="hint-rules">
                  {errors && t(errors.hint?.message)}
                </div>
                <div className='hint-length'>{hint?.length}/255</div>
              </div>
            </div>
          </form>
        </section>
      </main>
      <Footer
        backButton={
          <Button
            text={t('cancelButtonText')}
            color={COLORS.secondary}
            back
          />
        }
        forwardButton={
          status === HTTP_STATUS.loading
            ? <Spinner />
            : (
              <div
                className={`button ${COLORS.secondary} fill`}
                onClick={submitForm}
              >
                {t('nextButtonText')}
                <div className='icon-container'>
                  <FontAwesomeIcon
                    className='icon'
                    icon={faAngleRight}
                  />
                </div>
              </div>
            )
        }
      />
    </div >
  );
}

export default CreatePasswordForm;
