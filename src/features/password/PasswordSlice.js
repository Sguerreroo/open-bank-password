import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { submitForm } from '../../services/api';

import { incrementStep } from '../progressSteps/StepSlice';

import { HTTP_STATUS } from '../../constants/HttpStatus';

const initialState = {
  status: HTTP_STATUS.idle,
  password: null,
  hint: null,
};

export const submitPassword = createAsyncThunk(
  'password/submitPassword',
  async ({ password, hint }, thunkAPI) => {
    const { rejectWithValue, dispatch } = thunkAPI;
    try {
      const response = await submitForm(password);
      dispatch(incrementStep());
      return { ...response, password, hint };
    } catch (error) {
      dispatch(incrementStep());
      return rejectWithValue(error);
    }
  }
)

export const passwordManagerSlice = createSlice({
  name: 'passwordManager',
  initialState,
  reducers: {
    resetPasswordManager: () => initialState,
  },
  extraReducers: {
    [submitPassword.pending]: (state, action) => {
      state.status = HTTP_STATUS.loading;
    },
    [submitPassword.fulfilled]: (state, action) => {
      state.status = HTTP_STATUS.succeed;
      const { password, hint } = action.payload;
      state.password = password;
      state.hint = hint;
    },
    [submitPassword.rejected]: (state, action) => {
      state.status = HTTP_STATUS.failed;
    },
  }
});

export const { resetPasswordManager } = passwordManagerSlice.actions;

export default passwordManagerSlice.reducer;