import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import ConnectionBar from './ConnectionBar';
import StepNumber from './StepNumber';

import './StepItem.scss';

function StepItem({ number, current = false }) {

  const {
    currentStep, maxStep, minStep
  } = useSelector(state => state.step);

  const [passedStep, setPassedStep] = useState(false);

  // Save if this step is passed
  useEffect(() => {
    number < currentStep
      ? setPassedStep(true)
      : setPassedStep(false);
  }, [number, currentStep]);

  return (
    <div className='step-item'>
      <ConnectionBar
        visibility={number > minStep}
        passedStep={current || number < currentStep}
      />
      <StepNumber
        number={number}
        current={current}
        passedStep={passedStep}
      />
      <ConnectionBar
        visibility={number < maxStep}
        passedStep={passedStep}
      />
      {
        current && <div className='selected-item'></div>
      }
    </div>
  );
}

StepItem.propTypes = {
  number: PropTypes.number.isRequired,
}

export default StepItem;
