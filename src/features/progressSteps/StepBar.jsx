import React from 'react';
import { useSelector } from 'react-redux';

import StepItem from './StepItem';

import './StepBar.scss';

function StepBar() {

  const {
    currentStep, maxStep
  } = useSelector(state => state.step);

  return (
    <nav className='nav-bar'>
      <div className='step-bar-container'>
        {
          [...Array(maxStep).keys()].map(number => {
            const step = number + 1;
            return (
              <StepItem
                key={number}
                number={step}
                current={step === currentStep}
              />
            );
          })
        }
      </div>
    </nav>
  );
}

export default StepBar;
