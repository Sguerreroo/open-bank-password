import React from 'react';

import './ConnectionBar.scss';

function ConnectionBar({ visibility, passedStep }) {
  return (
    <div
      className={`
        connection-bar ${!visibility ? 'hidden' : ''}
        ${passedStep ? 'passed' : ''}
      `}
    ></div>
  );
}

export default ConnectionBar;
