import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

import './StepNumber.scss';

function StepNumber({ number, current, passedStep }) {
  return (
    <div
      className={`
        step-number ${current ? 'selected-number' : ''}
        ${passedStep ? 'passed' : ''}
      `}>
      {
        passedStep
          ? (
            <FontAwesomeIcon
              className='icon'
              icon={faCheck}
            />
          )
          : number
      }
    </div>
  );
}

export default StepNumber;
