import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  currentStep: 1,
  minStep: 1,
  maxStep: 3,
};

export const stepSlice = createSlice({
  name: 'step',
  initialState,
  reducers: {
    incrementStep: state => {
      state.currentStep + 1 > state.maxStep
        ? state.currentStep = 1
        : state.currentStep += 1;
    },
    decrementStep: state => {
      if (state.currentStep - 1 >= state.minStep)
        state.currentStep -= 1;
    },
  }
});

export const {
  incrementStep, decrementStep
} = stepSlice.actions;

export default stepSlice.reducer;