import { ROUTES } from "./Routes";

export const STEP_PAGE = {
  1: ROUTES.home,
  2: ROUTES.createPassword,
  3: ROUTES.feedback,
}