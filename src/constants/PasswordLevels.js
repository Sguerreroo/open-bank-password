export const PASSWORD_LEVELS = {
  'passwordOneNumber': {
    width: '25%',
    backgroundColor: 'red'
  },
  'passwordCapitalLetter': {
    width: '50%',
    backgroundColor: 'red'
  },
  'passwordMinChar': {
    width: '75%',
    backgroundColor: 'orange'
  },
}