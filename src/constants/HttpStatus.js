export const HTTP_STATUS = {
  idle: 'idle',
  succeed: 'succeed',
  failed: 'failed',
  loading: 'loading',
}