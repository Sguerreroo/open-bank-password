import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

import { changeLanguage } from '../../locale/languageSlice';

import { LANGUAGE_CODES } from '../../constants/LanguageCodes';

import './SubTitle.scss';

function SubTitle() {

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { selectedLanguage } = useSelector(state => state.language);

  useEffect(() => {
    if (selectedLanguage)
      i18next.changeLanguage(selectedLanguage);
  }, [selectedLanguage]);

  const selectLanguage = (lng) => {
    dispatch(changeLanguage(lng));
  };

  return (
    <div className='subtitle-container'>
      <div className='subtitle'>
        <div>
          <div className='underlined-word'>
            {t('createWord')}
          </div>
          <div className='underline'></div>
        </div>
        <div>
          {t('passwordManagerText')}
        </div>
      </div>
      <div className='button-language-change'>
        {
          selectedLanguage === LANGUAGE_CODES.EN
            ? (
              <div
                onClick={() => selectLanguage(LANGUAGE_CODES.ES)}
              >
                Español
              </div>
            )
            : (
              <div
                onClick={() => selectLanguage(LANGUAGE_CODES.EN)}
              >
                English
              </div>
            )
        }
      </div>
    </div>
  );
}

export default SubTitle;
