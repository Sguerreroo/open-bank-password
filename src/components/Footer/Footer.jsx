import React from 'react';

import './Footer.scss';

function Footer({ backButton, forwardButton }) {
  return (
    <footer>
      {backButton}
      {forwardButton}
    </footer>
  );
}

export default Footer;
