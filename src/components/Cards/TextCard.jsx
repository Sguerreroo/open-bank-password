import React from 'react';
import PropTypes from 'prop-types';

import './TextCard.scss';

function TextCard({ title, text }) {
  return (
    <div className='text-card-container'>
      <h4>{title}</h4>
      <p>{text}</p>
    </div>
  );
}

TextCard.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default TextCard;
