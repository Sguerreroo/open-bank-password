import React from 'react';
import PropTypes from 'prop-types';

import './ImageCard.scss';

function ImageCard({ image, imgAlt, text }) {
  return (
    <div className='image-card-container'>
      <img src={image} alt={imgAlt} />
      <p>{text}</p>
    </div>
  );
}

ImageCard.propTypes = {
  imgAlt: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default ImageCard;
