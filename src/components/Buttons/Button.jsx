import React from 'react';
import { useDispatch } from 'react-redux';

import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  decrementStep, incrementStep
} from '../../features/progressSteps/StepSlice';

import './Button.scss';

function Button({
  text, color, icon, fill = false, back = false, disabled,
  action
}) {

  const dispatch = useDispatch();

  const handleClick = () => {
    if (disabled) return;
    if (action) {
      dispatch(action());
      return;
    }
    back
      ? dispatch(decrementStep())
      : dispatch(incrementStep())
  };

  return (
    <div
      disabled={disabled}
      className={`
        button ${color} ${disabled && 'disabled'}
        ${fill && 'fill'}
      `}
      onClick={handleClick}
    >
      {text}
      {
        icon && (
          <div className='icon-container'>
            <FontAwesomeIcon
              className='icon'
              icon={icon}
            />
          </div>
        )

      }
    </div>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
}

export default Button;
